/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 30.4.15
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Example of the linear interpolation in joint coordinate.
 *      The interpolation will be done from current state to the default (all zeros) state.
 *      If linear interpolation in joint coordinates is feasible by a robot, it will be executed.
 *      Otherwise, error is printed.
 */


#include <ros/ros.h>
#include <moveit_capabilities_clopema/LinearJointPlanner.h>
#include <moveit/move_group_interface/move_group.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "interpolation_test");
    ros::NodeHandle node("~");
    ros::AsyncSpinner spinner(1);
    spinner.start();

    move_group_interface::MoveGroup g("all");

    robot_state::RobotState start = *g.getCurrentState();
    robot_state::RobotState goal(start);
    goal.setToDefaultValues();

    LinearJointPlanner ljp;

    trajectory_msgs::JointTrajectory msg = ljp.interpolate_joints(start, goal, 1.0);

    move_group_interface::MoveGroup::Plan plan;
    plan.trajectory_.joint_trajectory = msg;

    if(msg.points.empty()) {
        ROS_WARN_STREAM("Cannot interpolate");
        return -1;
    }

    bool suc = g.execute(plan);
    if (!suc) {
        ROS_WARN_STREAM("Cannot execute the trajectory");
        return -1;
    }

    return 0;
}
