/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 29.4.15
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Implementation of the Linear Joint Planner
 */

#include <moveit_capabilities_clopema/LinearJointPlanner.h>
#include <moveit_capabilities_clopema/CheckTrajectory.h>
#include <moveit/robot_state/conversions.h>

trajectory_msgs::JointTrajectory LinearJointPlanner::interpolate_joints(const robot_state::RobotState &start,
                                                                        const robot_state::RobotState &goal,
                                                                        double duration,
                                                                        double max_step) const {
    double t = 0.0;

    double max_dist = get_maximum_distance(start, goal);
    if (max_dist < max_step) { //In case start is the same as the goal
        max_dist = max_step;
    }
    double t_step = max_step / max_dist;

    trajectory_msgs::JointTrajectory msg;
    msg.joint_names = start.getVariableNames();

    robot_state::RobotState state(start);
    for (double t = 0.0; t < 1.0; t += t_step) {
        start.interpolate(goal, t, state);
        trajectory_msgs::JointTrajectoryPoint p = rs2trajectory_point(state, t * duration);
        msg.points.push_back(p);
    }

    start.interpolate(goal, 1.0, state);
    msg.points.push_back(rs2trajectory_point(state, duration));

    if (!check_collisions(msg, start)) {
        msg = trajectory_msgs::JointTrajectory();
    }

    return msg;
}

double LinearJointPlanner::get_maximum_distance(const robot_state::RobotState &a,
                                                const robot_state::RobotState &b) const {

    if (a.getVariableCount() != b.getVariableCount()) {
        return 0.0;
    }

    double max_value = 0.0;
    for (int i = 0; i < a.getVariableCount(); ++i) {
        double dist = a.getVariablePosition(i) - b.getVariablePosition(i);
        dist = fabs(dist);
        if (dist > max_value) {
            max_value = dist;
        }
    }

    return max_value;
}

trajectory_msgs::JointTrajectoryPoint LinearJointPlanner::rs2trajectory_point(
        const robot_state::RobotState &state, double t) const {
    trajectory_msgs::JointTrajectoryPoint p;

    int n = state.getVariableCount();
    p.positions.resize(n, 0.0);
    p.velocities.resize(n, 0.0);
    p.accelerations.resize(n, 0.0);
    p.effort.resize(n, 0.0);

    for (int i = 0; i < n; ++i) {
        p.positions[i] = state.getVariablePosition(i);
        p.velocities[i] = state.getVariableVelocity(i);
        p.accelerations[i] = state.getVariableAcceleration(i);
        p.effort[i] = state.getVariableEffort(i);
    }
    p.time_from_start = ros::Duration(t);

    return p;
}

bool LinearJointPlanner::check_collisions(const trajectory_msgs::JointTrajectory &msg,
                                          const robot_state::RobotState &start_state) const {
    moveit_capabilities_clopema::CheckTrajectory srv;
    srv.request.rtraj.joint_trajectory = msg;
    robot_state::robotStateToRobotStateMsg(start_state, srv.request.start_state);
    bool suc = ros::service::call("/moveit_check_trajectory", srv);

    if (!suc) {
        return false;
    }

    return srv.response.valid;
}
