/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 29.4.15
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Implementation of the trajectory collision service capability
 */

#include <moveit_capabilities_clopema/TrajectoryCollisionService.h>

move_group::TrajectoryCollisionService::TrajectoryCollisionService() : MoveGroupCapability("CheckTrajectory") {
};

void move_group::TrajectoryCollisionService::initialize() {
    ser_check_trajectory_ = root_node_handle_.advertiseService("/moveit_check_trajectory",
                                                               &TrajectoryCollisionService::cb_check_trajectory,
                                                               this);
};

bool move_group::TrajectoryCollisionService::cb_check_trajectory(
        moveit_capabilities_clopema::CheckTrajectory::Request &req,
        moveit_capabilities_clopema::CheckTrajectory::Response &res) {
    using std::vector;
    using std::string;
    vector<string> allow_collision_1 = req.enable_collision_1;
    vector<string> allow_collision_2 = req.enable_collision_2;

    planning_scene::PlanningScenePtr ps = planning_scene::PlanningScene::clone(
            planning_scene_monitor::LockedPlanningSceneRW(context_->planning_scene_monitor_));

    for (unsigned int i = 0; i < allow_collision_1.size(); ++i) {
        ps->getAllowedCollisionMatrixNonConst().setEntry(allow_collision_1[i], allow_collision_2[i], true);
    }

    bool valid = ps->isPathValid(req.start_state, req.rtraj, "", true);

    for (unsigned int i = 0; i < allow_collision_1.size(); ++i) {
        ps->getAllowedCollisionMatrixNonConst().setEntry(allow_collision_1[i], allow_collision_2[i], false);
    }

    res.valid = valid;
    return true;
};

#include <class_loader/class_loader.h>

CLASS_LOADER_REGISTER_CLASS(move_group::TrajectoryCollisionService, move_group::MoveGroupCapability)
