cmake_minimum_required(VERSION 2.8.3)
project(moveit_capabilities_clopema)

find_package(catkin REQUIRED COMPONENTS
    moveit_core
    moveit_msgs
    moveit_ros_move_group
    moveit_ros_planning_interface # for the example only
    roscpp
    message_generation
)

add_service_files(
    DIRECTORY srv
    FILES
        CheckTrajectory.srv
)

generate_messages(DEPENDENCIES moveit_msgs)

catkin_package(
    INCLUDE_DIRS
        include
    LIBRARIES
        moveit_capabilities_clopema
        lib_linear_joint_planner
    CATKIN_DEPENDS
        moveit_core
        moveit_msgs
        moveit_ros_move_group
        roscpp
        message_runtime
    DEPENDS
)

include_directories(
    ${catkin_INCLUDE_DIRS}
    include
)

add_library(lib_linear_joint_planner
    src/LinearJointPlanner.cpp
)
target_link_libraries(lib_linear_joint_planner
    ${catkin_LIBRARIES}
)
add_dependencies(lib_linear_joint_planner
    ${catkin_EXPORTED_TARGETS}
)

add_library(moveit_capabilities_clopema
    src/TrajectoryCollisionService.cpp
)
target_link_libraries(moveit_capabilities_clopema
    ${catkin_LIBRARIES}
)
add_dependencies(moveit_capabilities_clopema
    ${catkin_EXPORTED_TARGETS}
)

add_executable(example_joint_interpolation
    src/example_joint_interpolation.cpp
)
target_link_libraries(example_joint_interpolation
    lib_linear_joint_planner
)

install(TARGETS moveit_capabilities_clopema
    ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
    LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
    RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(FILES
    moveit_capabilities_clopema.xml
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
