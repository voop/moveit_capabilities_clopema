
# Trajectory Collision Service Capability #

## Capability Installation ##

 - Put the package into your workspace and compile
 - Edit your **MoveIt!** configuration (file: *{robot}_moveit_config/launch/move_group.launch*) to contains:
    ``` move_group/TrajectoryCollisionService ```
    in the capabilities parameter.
 - Compile and run your robot (virtual/demo is enough)
 - Now, you should be able to see service named */moveit_check_trajectory*
 
## Linear Interpolation in Joint Coordinates ##

The joint interpolation in joint coordinates are used to demonstrate the trajectory checker interface.
To use the interpolation look at the example in *src/interpolation.cpp*.
