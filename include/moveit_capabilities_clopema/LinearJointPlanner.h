/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 29.4.15
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Linear Joint Planner Example as a demonstration of the trajectory collision service check
 */

#ifndef PROJECT_LINEARJOINTPLANNER_H
#define PROJECT_LINEARJOINTPLANNER_H

#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <moveit/robot_state/robot_state.h>

class LinearJointPlanner {

public:

    /** \brief Interpolate in joint coordinates between the start state \e start and the goal state \e goal.
     *  \param duration trajectory duration in second
     *  \param max_step maximum allowed step for joints in radians
     *  \return Empty trajectory if cannot find valid trajectory */
    trajectory_msgs::JointTrajectory interpolate_joints(const robot_state::RobotState &start,
                                                        const robot_state::RobotState &goal,
                                                        double duration = 10.0,
                                                        double max_step = 0.01) const;

    /** \brief Estimate maximum joint distance between states \e a and \e b */
    double get_maximum_distance(const robot_state::RobotState &a, const robot_state::RobotState &b) const;

    /** \brief Copy robot state to joint trajectory point and mark with time from start \e t in second */
    trajectory_msgs::JointTrajectoryPoint rs2trajectory_point(const robot_state::RobotState &state,
                                                              double t = 0.0) const;

    /** \brief Check trajectory for collisions via service of moveit capability.
     *  \return true if trajectory is valid */
    bool check_collisions(const trajectory_msgs::JointTrajectory &msg,
                          const robot_state::RobotState &start_state) const;

};


#endif //PROJECT_LINEARJOINTPLANNER_H
