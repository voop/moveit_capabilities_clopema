/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 29.4.15
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *    Details: Trajectory Collision Service Capability
 */

#ifndef PROJECT_TRAJECTORYCOLLISIONSERVICE_H
#define PROJECT_TRAJECTORYCOLLISIONSERVICE_H

#include <moveit/move_group/move_group_capability.h>
#include <moveit_capabilities_clopema/CheckTrajectory.h>

namespace move_group {

    class TrajectoryCollisionService : public MoveGroupCapability {

    public:
        /** \brief Construct the capability */
        TrajectoryCollisionService();

        /** \brief Initialize the service */
        virtual void initialize();

    private:
        /** \brief service callback function */
        bool cb_check_trajectory(moveit_capabilities_clopema::CheckTrajectory::Request &req,
                                 moveit_capabilities_clopema::CheckTrajectory::Response &res);

    private:

        ros::ServiceServer ser_check_trajectory_;
    };

}


#endif //PROJECT_TRAJECTORYCOLLISIONSERVICE_H
